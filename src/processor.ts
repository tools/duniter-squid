import * as ss58 from "@subsquid/ss58"
import {EventHandlerContext, Store, SubstrateProcessor} from "@subsquid/substrate-processor"
import {Account, HistoricalBalance, Identity, Ud} from "./model"
import {BalancesDepositEvent, BalancesTransferEvent, IdentityIdtyCreatedEvent, IdentityIdtyRemovedEvent, IdentityIdtyValidatedEvent, UniversalDividendNewUdCreatedEvent} from "./types/events"


const processor = new SubstrateProcessor('duniter-squid')


processor.setBatchSize(500)


processor.setDataSource({
    archive: 'http://localhost:4010/v1/graphql',
    chain: 'ws://127.0.0.1:9944'
})

processor.addPreHook(async ctx => {
    if (ctx.block.height == 1) {
        // TODO: fill genesis data
    }
})

processor.addEventHandler('balances.Deposit', async ctx => {
    let deposit = getDepositEvent(ctx)
    let who = ss58.codec(42).encode(deposit.who)

    let whoAcc = await getOrCreate(ctx.store, Account, who)
    whoAcc.balance = whoAcc.balance || 0n
    whoAcc.balance += deposit.amount
    await ctx.store.save(whoAcc)

    await ctx.store.save(new HistoricalBalance({
        id: ctx.event.id + '-who',
        account: whoAcc,
        balance: whoAcc.balance,
        date: new Date(ctx.block.timestamp)
    }))
})

processor.addEventHandler('balances.Transfer', async ctx => {
    let transfer = getTransferEvent(ctx)
    let tip = ctx.extrinsic?.tip || 0n
    let from = ss58.codec(42).encode(transfer.from)
    let to = ss58.codec(42).encode(transfer.to)

    let fromAcc = await getOrCreate(ctx.store, Account, from)
    fromAcc.balance = fromAcc.balance || 0n
    fromAcc.balance -= transfer.amount
    fromAcc.balance -= tip
    await ctx.store.save(fromAcc)

    const toAcc = await getOrCreate(ctx.store, Account, to)
    toAcc.balance = toAcc.balance || 0n
    toAcc.balance += transfer.amount
    await ctx.store.save(toAcc)

    await ctx.store.save(new HistoricalBalance({
        id: ctx.event.id + '-to',
        account: fromAcc,
        balance: fromAcc.balance,
        date: new Date(ctx.block.timestamp)
    }))

    await ctx.store.save(new HistoricalBalance({
        id: ctx.event.id + '-from',
        account: toAcc,
        balance: toAcc.balance,
        date: new Date(ctx.block.timestamp)
    }))
})

processor.addEventHandler('identity.IdtyCreated', async ctx => {
    let event = getIdtyCreatedEvent(ctx)
    let ownerKey = ss58.codec(42).encode(event.ownerKey)
    let blockNumber = ctx.block.height;

    let ownerAccount = await getOrCreate(ctx.store, Account, ownerKey)
    if (!ownerAccount.balance) {
        ownerAccount.balance = 0n;
    }
    await ctx.store.save(ownerAccount)
    let entity = await getOrCreate(ctx.store, Identity, event.idtyIndex.toString())
    entity.account = ownerAccount;
    entity.idtyIndex = event.idtyIndex;
    entity.createdAt = blockNumber;
    await ctx.store.save(entity)
})

processor.addEventHandler('identity.IdtyValidated', async ctx => {
    let event = getIdtyValidatedEvent(ctx)
    let blockNumber = ctx.block.height;

    let entity = await getOrCreate(ctx.store, Identity, event.idtyIndex.toString())
    entity.validatedAt = blockNumber;
    await ctx.store.save(entity)
})

processor.addEventHandler('identity.IdtyRemoved', async ctx => {
    let event = getIdtyRemovedEvent(ctx)
    let blockNumber = ctx.block.height;

    let entity = await getOrCreate(ctx.store, Identity, event.idtyIndex.toString())
    entity.removedAt = blockNumber;
    await ctx.store.save(entity)
})

processor.addEventHandler('universalDividend.NewUdCreated', async ctx => {
    let ud = getUdCreatedEvent(ctx)
    let blockNumber = ctx.block.height;

    let udEntity = await getOrCreate(ctx.store, Ud, blockNumber.toString())
    udEntity.block = blockNumber;
    udEntity.amount = ud.amount
    udEntity.date = new Date(ctx.block.timestamp);
    await ctx.store.save(udEntity)
})

processor.run()

// EVENTS //

// Deposit
interface DepositEvent {
    who: Uint8Array
    amount: bigint
}
function getDepositEvent(ctx: EventHandlerContext): DepositEvent {
    let event = new BalancesDepositEvent(ctx)
    return event.asV100
}

// IdtyCreated
interface IdtyCreatedEvent {
    idtyIndex: number
    ownerKey: Uint8Array
}
function getIdtyCreatedEvent(ctx: EventHandlerContext): IdtyCreatedEvent {
    let event = new IdentityIdtyCreatedEvent(ctx)
    return event.asV100
}

// IdtyValidated
interface IdtyValidatedEvent {
    idtyIndex: number
}
function getIdtyValidatedEvent(ctx: EventHandlerContext): IdtyValidatedEvent {
    let event = new IdentityIdtyValidatedEvent(ctx)
    return event.asV100
}

// IdtyRemoved
interface IdtyRemovedEvent {
    idtyIndex: number
}
function getIdtyRemovedEvent(ctx: EventHandlerContext): IdtyRemovedEvent {
    let event = new IdentityIdtyRemovedEvent(ctx)
    return event.asV100
}

//Transfer
interface TransferEvent {
    from: Uint8Array
    to: Uint8Array
    amount: bigint
}
function getTransferEvent(ctx: EventHandlerContext): TransferEvent {
    let event = new BalancesTransferEvent(ctx)
    return event.asV100
}

// UdCreated
interface UdCreatedEvent {
    amount: bigint
    membersCount: bigint
}
function getUdCreatedEvent(ctx: EventHandlerContext): UdCreatedEvent {
    let event = new UniversalDividendNewUdCreatedEvent(ctx)
    return event.asV100
}

async function getOrCreate<T extends {id: string}>(
    store: Store,
    entityConstructor: EntityConstructor<T>,
    id: string
): Promise<T> {

    let e = await store.get<T>(entityConstructor, {
        where: { id },
    })

    if (e == null) {
        e = new entityConstructor()
        e.id = id
    }

    return e
}


type EntityConstructor<T> = {
    new (...args: any[]): T
}
