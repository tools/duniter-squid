import {Entity as Entity_, Column as Column_, PrimaryColumn as PrimaryColumn_, ManyToOne as ManyToOne_, Index as Index_} from "typeorm"
import {Account} from "./account.model"

@Entity_()
export class Identity {
  constructor(props?: Partial<Identity>) {
    Object.assign(this, props)
  }

  @PrimaryColumn_()
  id!: string

  /**
   * Identity
   */
  @Index_()
  @ManyToOne_(() => Account, {nullable: false})
  account!: Account

  @Index_()
  @Column_("integer", {nullable: false})
  idtyIndex!: number

  @Column_("integer", {nullable: false})
  createdAt!: number

  @Column_("integer", {nullable: true})
  validatedAt!: number | undefined | null

  @Column_("integer", {nullable: true})
  removedAt!: number | undefined | null
}
