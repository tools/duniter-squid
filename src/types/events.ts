import assert from 'assert'
import {EventContext, Result, deprecateLatest} from './support'
import * as v100 from './v100'

export class BalancesDepositEvent {
  constructor(private ctx: EventContext) {
    assert(this.ctx.event.name === 'balances.Deposit')
  }

  /**
   * Some amount was deposited (e.g. for transaction fees).
   */
  get isV100(): boolean {
    return this.ctx._chain.getEventHash('balances.Deposit') === '2fc945270cdc964bc687ae2dcbf54becda7b067c05e2651f29e0a4fbe92a0438'
  }

  /**
   * Some amount was deposited (e.g. for transaction fees).
   */
  get asV100(): {who: v100.AccountId32, amount: bigint} {
    assert(this.isV100)
    return this.ctx._chain.decodeEvent(this.ctx.event)
  }

  get isLatest(): boolean {
    deprecateLatest()
    return this.isV100
  }

  get asLatest(): {who: v100.AccountId32, amount: bigint} {
    deprecateLatest()
    return this.asV100
  }
}

export class BalancesEndowedEvent {
  constructor(private ctx: EventContext) {
    assert(this.ctx.event.name === 'balances.Endowed')
  }

  /**
   * An account was created with some free balance.
   */
  get isV100(): boolean {
    return this.ctx._chain.getEventHash('balances.Endowed') === 'abff717d931cb0e0aaef6b38a35cf55200ddc21f19bb42d1f8396ee910183841'
  }

  /**
   * An account was created with some free balance.
   */
  get asV100(): {account: v100.AccountId32, freeBalance: bigint} {
    assert(this.isV100)
    return this.ctx._chain.decodeEvent(this.ctx.event)
  }

  get isLatest(): boolean {
    deprecateLatest()
    return this.isV100
  }

  get asLatest(): {account: v100.AccountId32, freeBalance: bigint} {
    deprecateLatest()
    return this.asV100
  }
}

export class BalancesTransferEvent {
  constructor(private ctx: EventContext) {
    assert(this.ctx.event.name === 'balances.Transfer')
  }

  /**
   * Transfer succeeded.
   */
  get isV100(): boolean {
    return this.ctx._chain.getEventHash('balances.Transfer') === 'df170dc34b34af0f9539e2548a0e40b1909a9738b3e7699d6badd150723f6783'
  }

  /**
   * Transfer succeeded.
   */
  get asV100(): {from: v100.AccountId32, to: v100.AccountId32, amount: bigint} {
    assert(this.isV100)
    return this.ctx._chain.decodeEvent(this.ctx.event)
  }

  get isLatest(): boolean {
    deprecateLatest()
    return this.isV100
  }

  get asLatest(): {from: v100.AccountId32, to: v100.AccountId32, amount: bigint} {
    deprecateLatest()
    return this.asV100
  }
}

export class IdentityIdtyCreatedEvent {
  constructor(private ctx: EventContext) {
    assert(this.ctx.event.name === 'identity.IdtyCreated')
  }

  /**
   * A new identity has been created
   * [idty_index, owner_key]
   */
  get isV100(): boolean {
    return this.ctx._chain.getEventHash('identity.IdtyCreated') === 'c68075fcb410ce9cb6c40279b2d59e7300c68cd0f1d2688ffef5015571b003e2'
  }

  /**
   * A new identity has been created
   * [idty_index, owner_key]
   */
  get asV100(): {idtyIndex: number, ownerKey: v100.AccountId32} {
    assert(this.isV100)
    return this.ctx._chain.decodeEvent(this.ctx.event)
  }

  get isLatest(): boolean {
    deprecateLatest()
    return this.isV100
  }

  get asLatest(): {idtyIndex: number, ownerKey: v100.AccountId32} {
    deprecateLatest()
    return this.asV100
  }
}

export class IdentityIdtyRemovedEvent {
  constructor(private ctx: EventContext) {
    assert(this.ctx.event.name === 'identity.IdtyRemoved')
  }

  /**
   * An identity has been removed
   * [idty_index]
   */
  get isV100(): boolean {
    return this.ctx._chain.getEventHash('identity.IdtyRemoved') === '8a86d9f70241ee6da2cd4e465d99ef35a6d57cb7629660541ecf82cf78e77f0f'
  }

  /**
   * An identity has been removed
   * [idty_index]
   */
  get asV100(): {idtyIndex: number} {
    assert(this.isV100)
    return this.ctx._chain.decodeEvent(this.ctx.event)
  }

  get isLatest(): boolean {
    deprecateLatest()
    return this.isV100
  }

  get asLatest(): {idtyIndex: number} {
    deprecateLatest()
    return this.asV100
  }
}

export class IdentityIdtyValidatedEvent {
  constructor(private ctx: EventContext) {
    assert(this.ctx.event.name === 'identity.IdtyValidated')
  }

  /**
   * An identity has been validated
   * [idty_index]
   */
  get isV100(): boolean {
    return this.ctx._chain.getEventHash('identity.IdtyValidated') === '6c6d45bc9a064d8e3a42854153b02ec26b374ea634f2d190d86a1e7610176158'
  }

  /**
   * An identity has been validated
   * [idty_index]
   */
  get asV100(): {idtyIndex: number} {
    assert(this.isV100)
    return this.ctx._chain.decodeEvent(this.ctx.event)
  }

  get isLatest(): boolean {
    deprecateLatest()
    return this.isV100
  }

  get asLatest(): {idtyIndex: number} {
    deprecateLatest()
    return this.asV100
  }
}

export class UniversalDividendNewUdCreatedEvent {
  constructor(private ctx: EventContext) {
    assert(this.ctx.event.name === 'universalDividend.NewUdCreated')
  }

  /**
   * A new universal dividend is created
   * [amout, members_count]
   */
  get isV100(): boolean {
    return this.ctx._chain.getEventHash('universalDividend.NewUdCreated') === 'af36392629156183a2d9292800d19becfd04aa639c0ad2eaccfc79b964bb0692'
  }

  /**
   * A new universal dividend is created
   * [amout, members_count]
   */
  get asV100(): {amount: bigint, monetaryMass: bigint, membersCount: bigint} {
    assert(this.isV100)
    return this.ctx._chain.decodeEvent(this.ctx.event)
  }

  get isLatest(): boolean {
    deprecateLatest()
    return this.isV100
  }

  get asLatest(): {amount: bigint, monetaryMass: bigint, membersCount: bigint} {
    deprecateLatest()
    return this.asV100
  }
}
