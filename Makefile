process: migrate
	@node -r dotenv/config lib/processor.js


serve:
	@npx squid-graphql-server


migrate:
	@npx sqd db:migrate


migration:
	@npx sqd db:create-migration Data


build:
	@npm run build


codegen:
	@npx sqd codegen


typegen: duniterVersions.json
	@npx squid-substrate-typegen typegen.json


duniterVersions.json:
	@make explore


explore:
	@npx squid-substrate-metadata-explorer \
		--chain ws://127.0.0.1:9944 \
		--archive http://localhost:4010/v1/graphql \
		--out duniterVersions.json


up:
	@docker-compose up -d


down:
	@docker-compose down


.PHONY: process serve start codegen migration migrate up down
