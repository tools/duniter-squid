module.exports = class Init1644788361836 {
  name = 'Init1644788361836'

  async up(db) {
    await db.query(`CREATE TABLE "historical_balance" ("id" character varying NOT NULL, "balance" numeric NOT NULL, "date" TIMESTAMP WITH TIME ZONE NOT NULL, "account_id" character varying NOT NULL, CONSTRAINT "PK_74ac29ad0bdffb6d1281a1e17e8" PRIMARY KEY ("id"))`)
    await db.query(`CREATE INDEX "IDX_383ff006e4b59db91d32cb891e" ON "historical_balance" ("account_id") `)
    await db.query(`CREATE TABLE "account" ("id" character varying NOT NULL, "balance" numeric NOT NULL, CONSTRAINT "PK_54115ee388cdb6d86bb4bf5b2ea" PRIMARY KEY ("id"))`)
    await db.query(`CREATE TABLE "identity" ("id" character varying NOT NULL, "idty_index" integer NOT NULL, "created_at" integer NOT NULL, "validated_at" integer, "removed_at" integer, "account_id" character varying NOT NULL, CONSTRAINT "PK_ff16a44186b286d5e626178f726" PRIMARY KEY ("id"))`)
    await db.query(`CREATE INDEX "IDX_bafa9e6c71c3f69cef6602a809" ON "identity" ("account_id") `)
    await db.query(`CREATE INDEX "IDX_bebbf78647357e8fe9e35caaab" ON "identity" ("idty_index") `)
    await db.query(`CREATE TABLE "ud" ("id" character varying NOT NULL, "block" integer NOT NULL, "amount" numeric NOT NULL, "date" TIMESTAMP WITH TIME ZONE NOT NULL, CONSTRAINT "PK_66c6139856b531b64a8071b33dd" PRIMARY KEY ("id"))`)
    await db.query(`CREATE INDEX "IDX_8911a8b0a2d12d0139a6c21965" ON "ud" ("block") `)
    await db.query(`ALTER TABLE "historical_balance" ADD CONSTRAINT "FK_383ff006e4b59db91d32cb891e9" FOREIGN KEY ("account_id") REFERENCES "account"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`)
    await db.query(`ALTER TABLE "identity" ADD CONSTRAINT "FK_bafa9e6c71c3f69cef6602a8095" FOREIGN KEY ("account_id") REFERENCES "account"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`)
  }

  async down(db) {
    await db.query(`DROP TABLE "historical_balance"`)
    await db.query(`DROP INDEX "public"."IDX_383ff006e4b59db91d32cb891e"`)
    await db.query(`DROP TABLE "account"`)
    await db.query(`DROP TABLE "identity"`)
    await db.query(`DROP INDEX "public"."IDX_bafa9e6c71c3f69cef6602a809"`)
    await db.query(`DROP INDEX "public"."IDX_bebbf78647357e8fe9e35caaab"`)
    await db.query(`DROP TABLE "ud"`)
    await db.query(`DROP INDEX "public"."IDX_8911a8b0a2d12d0139a6c21965"`)
    await db.query(`ALTER TABLE "historical_balance" DROP CONSTRAINT "FK_383ff006e4b59db91d32cb891e9"`)
    await db.query(`ALTER TABLE "identity" DROP CONSTRAINT "FK_bafa9e6c71c3f69cef6602a8095"`)
  }
}
